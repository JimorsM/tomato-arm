<!DOCTYPE html>
<!--
Tomato GUI
Copyright (C) 2006-2010 Jonathan Zarate
http://www.polarcloud.com/tomato/

For use with Tomato Firmware only.
No part of this file may be used without permission.
-->
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<meta name="robots" content="noindex,nofollow">

		<title>[<% ident(); %>] <% translate("Restarting"); %>...</title>
		<style>
			body {
				font-family: Verdana;
				font-size: 14px;
				background: #bfbfbf;
				color: #585858;
				line-height: 2em;
			}

			#loader {
				width:95%;
				max-width: 600px;
				margin: 15% auto;
				padding: 15px;
				text-align: center;
			}

			/* Pure CSS preloader */
			.spinner {
				display: inline-block;
				width: 36px;
				height: 36px;
				box-sizing: border-box;
				vertical-align: middle;
				border: solid 2px transparent;
				border-top-color: #3C3C3C;
				border-bottom-color: #3C3C3C;
				border-radius: 50%;
				-webkit-border-radius: 50%;
				-webkit-animation: tomato-spinner 600ms linear infinite;
				animation: tomato-spinner 600ms linear infinite; 
			}
			.btn {
				font-family: Verdana;
				display: inline-block;
				text-align: center;
				cursor: pointer;
				background-image: none;
				padding: 5px 16px;
				margin: 0;
				font-size: 13px;
				font-weight: bold;
				line-height: 1.42857143;
				color: #555 !important;
				background: #f0f0f0;
				transition: 0.1s ease-out;
				border-radius: 4px;
				-webkit-border-radius: 4px;
				border: 1px solid #706e6e;
			}
			.btn:hover {
				color: #fff !important;
				background: #555;
				border: 1px solid #555;
			}
			.btn:active, .btn:focus {
				transition: none;
				border-width: 1px;
			}

			@-webkit-keyframes tomato-spinner { 0%   { -webkit-transform: rotate(0deg); }  100% { -webkit-transform: rotate(360deg); } }
			@keyframes tomato-spinner { 0%   { transform: rotate(0deg); }  100% { transform: rotate(360deg); } }
		</style>
		<script language='javascript'>
			var n = 20;
			function tick()
			{
				var e = document.getElementById('continue');
				e.innerHTML = n;
				if (n == 10) {
					e.disabled = false;
				}
				if (n == 0) {
					e.innerHTML = '<% translate("Continue"); %>';
				}
				else {
					--n;
					setTimeout(tick, 1000);
				}
			}
			function go()
			{
				window.location = window.location.protocol + '//<% nv("lan_ipaddr"); %>/';
			}
		</script>
	</head>
	<body onload="tick()" onclick="go()">

		<div id="loader">
			<div class="spinner"></div><br/><br/>
			<% translate("The router's new IP address is"); %> <b><% nv("lan_ipaddr"); %></b><br/>
			<% translate("You may need to release then renew your computer's DHCP lease before continuing"); %>.
			<br/><br/>
			<% translate("Please wait while the router restarts"); %>... &nbsp;
			<button class="btn" id="continue" onclick="go()" disabled><% translate("Continue"); %></button>
		</div>

	</body>
</html>
