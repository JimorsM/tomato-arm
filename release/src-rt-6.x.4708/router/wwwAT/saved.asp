<!DOCTYPE html>
<html lang="en">
	<!--
	Tomato GUI
	Copyright (C) 2006-2010 Jonathan Zarate
	http://www.polarcloud.com/tomato/

	For use with Tomato Firmware only.
	No part of this file may be used without permission.
	-->
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<meta name="robots" content="noindex,nofollow">
		<title>[<% ident(); %>] <% translate("Tomato"); %></title>
		<style>
			body {
				font-family: Verdana;
				font-size: 14px;
				background: #bfbfbf;
				color: #585858;
				line-height: 2em;
			}

			#loader {
				width:100%;
				max-width: 250px;
				text-align: center;
				margin: 15% auto;
				padding: 20px;
			}

			.btn {
				font-family: Verdana;
				display: inline-block;
				text-align: center;
				cursor: pointer;
				background-image: none;
				padding: 5px 16px;
				margin: 0;
				font-size: 13px;
				font-weight: bold;
				line-height: 1.42857143;
				color: #555 !important;
				background: #f0f0f0;
				transition: 0.1s ease-out;
				border-radius: 4px;
				-webkit-border-radius: 4px;
				border: 1px solid #706e6e;
			}
			.btn:hover {
				color: #fff !important;
				background: #555;
				border: 1px solid #555;
			}
			.btn:active, .btn:focus {
				transition: none;
				border-width: 1px;
			}

			/* Pure CSS preloader */
			.spinner {
				display: inline-block;
				width: 36px;
				height: 36px;
				box-sizing: border-box;
				vertical-align: middle;
				border: solid 2px transparent;
				border-top-color: #3C3C3C;
				border-bottom-color: #3C3C3C;
				border-radius: 50%;
				-webkit-border-radius: 50%;
				-webkit-animation: tomato-spinner 600ms linear infinite;
				animation: tomato-spinner 600ms linear infinite;
			}
			@-webkit-keyframes tomato-spinner { 0%   { -webkit-transform: rotate(0deg); }  100% { -webkit-transform: rotate(360deg); } }
			@keyframes tomato-spinner { 0%   { transform: rotate(0deg); }  100% { transform: rotate(360deg); } }
		</style>
		<script language="javascript">
			wait = parseInt('<% cgi_get("_nextwait"); %>', 10);
			if (isNaN(wait)) wait = 5;
			function tick()
			{
				clock.innerHTML = wait;

				if (--wait >= 0) setTimeout(tick, 1000);
				else go();
			}
			function go()
			{
				clock.style.visibility = 'hidden';
				window.location.replace('<% cgi_get("_nextpage"); %>');
			}
			function setSpin(x)
			{
				document.getElementById('spin').style.visibility = x ? 'visible' : 'hidden';
				spun = x;
			}
			function init()
			{
				if (wait > 0) {
					spin = document.getElementById('spin');
					opacity = 1;
					step = 1 / wait;
					clock = document.getElementById('xclock');
					clock.style.visibility = 'visible';
					tick();
				}
			}
		</script>
	</head>
	<body onload="init()" onclick="go()">

		<div id="loader">
			<script type="text/javascript">
				if (wait <= 0) s = '<% translate("Changes Saved"); %>!<br/><br/> <button onclick="go()" class="btn"><% translate("Continue"); %></button>';
				else s = '<div class="spinner"></div><br/><br/><% translate("Working, please wait"); %> <span id="xclock" style="visibility:hidden">&nbsp;</span> <% translate("s."); %>';
				document.write(s);
			</script>
		</div>

	</body>
</html>
